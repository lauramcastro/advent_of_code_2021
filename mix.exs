defmodule AdventOfCode2021.MixProject do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      version: "0.1.0",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "My take on Advent of Code 2021",
      source_url: "https://gitlab.com/lauramcastro/advent_of_code_2021",
      homepage_url: "http://gitlab.com/lauramcastro/advent_of_code_2021",
      docs: [
        extras: ["README.md"]
      ],

      # Coverage
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
    ]
  end

  # Dependencies listed here are available only for this
  # project and cannot be accessed from applications inside
  # the apps folder.
  #
  # Run "mix help deps" for examples and options.
  defp deps do
    [
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.1", only: [:dev], runtime: false},
      {:ex_doc, "~> 0.26", only: :dev, runtime: false},
      {:excoveralls, "~> 0.14", only: :test}
    ]
  end
end
