defmodule Advent01 do
  @moduledoc """
  Documentation for `Advent01`.
  """

  @doc """
  Counts the number of times a depth measurement increases from the previous.

  ## Examples

      iex> Advent01.increasing_depths([199,200,208,210,200,207,240,269,260,263])
      7

  """
  @spec increasing_depths(list(integer())) :: non_neg_integer()
  def increasing_depths(measurements) do
    do_increasing_depths(measurements, 0)
  end

  defp do_increasing_depths([_h1], n), do: n

  defp do_increasing_depths([h1, h2 | t], n) when h2 > h1,
    do: do_increasing_depths([h2 | t], n + 1)

  defp do_increasing_depths([_h1, h2 | t], n), do: do_increasing_depths([h2 | t], n)

  @doc """
  Counts the number of times a three-measurement sliding window increases from the previous.

  ## Examples

      iex> Advent01.increasing_depths_with_window([199,200,208,210,200,207,240,269,260,263])
      5

  """
  @spec increasing_depths_with_window(list(integer())) :: non_neg_integer()
  def increasing_depths_with_window(measurements) do
    do_increasing_depths_with_window(measurements, 0)
  end

  defp do_increasing_depths_with_window([_h1, _h2, _h3], n), do: n

  defp do_increasing_depths_with_window([h1, h2, h3, h4 | t], n) when h2 + h3 + h4 > h1 + h2 + h3,
    do: do_increasing_depths_with_window([h2, h3, h4 | t], n + 1)

  defp do_increasing_depths_with_window([_h1, h2, h3, h4 | t], n),
    do: do_increasing_depths_with_window([h2, h3, h4 | t], n)
end
