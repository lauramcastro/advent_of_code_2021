defmodule Advent01Test do
  use ExUnit.Case
  doctest Advent01

  test "answer to first challenge" do
    measurements = Utils.get_input("priv/input", fn x -> String.to_integer(x) end)

    assert Advent01.increasing_depths(measurements) == 1_752
  end

  test "answer to second challenge" do
    measurements = Utils.get_input("priv/input", fn x -> String.to_integer(x) end)

    assert Advent01.increasing_depths_with_window(measurements) == 1_781
  end
end
