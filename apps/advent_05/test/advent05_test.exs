defmodule Advent05Test do
  use ExUnit.Case
  doctest Advent05

  @tag :skip
  test "answer to first challenge" do
    linespecs = Utils.get_input("priv/input")
    assert Advent05.overlapping_points(linespecs) == 7_674
  end

  @tag :skip
  test "answer to second challenge" do
    linespecs = Utils.get_input("priv/input")
    assert Advent05.overlapping_points(linespecs, false) == 20_898
  end
end
