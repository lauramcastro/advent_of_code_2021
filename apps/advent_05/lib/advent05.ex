defmodule Advent05 do
  @moduledoc """
  Documentation for `Advent05`.
  """

  @doc """
  Examine a list of line specs, and report the account of double overlapping.

  ## Examples

      iex> Advent05.overlapping_points(["0,9 -> 5,9",
      ...>                              "8,0 -> 0,8",
      ...>                              "9,4 -> 3,4",
      ...>                              "2,2 -> 2,1",
      ...>                              "7,0 -> 7,4",
      ...>                              "6,4 -> 2,0",
      ...>                              "0,9 -> 2,9",
      ...>                              "3,4 -> 1,4",
      ...>                              "0,0 -> 8,8",
      ...>                              "5,5 -> 8,2"])
      5

      iex> Advent05.overlapping_points(["0,9 -> 5,9",
      ...>                              "8,0 -> 0,8",
      ...>                              "9,4 -> 3,4",
      ...>                              "2,2 -> 2,1",
      ...>                              "7,0 -> 7,4",
      ...>                              "6,4 -> 2,0",
      ...>                              "0,9 -> 2,9",
      ...>                              "3,4 -> 1,4",
      ...>                              "0,0 -> 8,8",
      ...>                              "5,5 -> 8,2"], false)
      12

  """
  @spec overlapping_points(list(String.t()), boolean()) :: integer()
  def overlapping_points(linespecs, no_diagonals \\ true) do
    parse_lines(linespecs, [])
    |> filter_diagonals(no_diagonals, [])
    |> to_points([])
    |> keep_duplicates([])
    |> Enum.uniq()
    |> length
  end

  defp parse_lines([], acc), do: acc

  defp parse_lines([c_str | more], acc) do
    [orig, dest] = String.split(c_str, " -> ")
    [x1, y1] = String.split(orig, ",")
    [x2, y2] = String.split(dest, ",")

    parse_lines(more, [
      {{String.to_integer(x1), String.to_integer(y1)},
       {String.to_integer(x2), String.to_integer(y2)}}
      | acc
    ])
  end

  defp filter_diagonals(lines, false, []), do: lines

  defp filter_diagonals([], true, acc), do: acc

  defp filter_diagonals([c = {{x, _}, {x, _}} | rest], true, acc),
    do: filter_diagonals(rest, true, [c | acc])

  defp filter_diagonals([c = {{_, y}, {_, y}} | rest], true, acc),
    do: filter_diagonals(rest, true, [c | acc])

  defp filter_diagonals([_ | rest], true, acc), do: filter_diagonals(rest, true, acc)

  defp to_points([], acc), do: acc
  # end of line
  defp to_points([{p, p} | rest], acc), do: to_points(rest, [p | acc])
  # horizontal line (onwards)
  defp to_points([{{x, y1}, {x, y2}} | rest], acc) when y1 < y2,
    do: to_points([{{x, y1 + 1}, {x, y2}} | rest], [{x, y1} | acc])

  # horizontal line (backwards)
  defp to_points([{{x, y1}, {x, y2}} | rest], acc),
    do: to_points([{{x, y1 - 1}, {x, y2}} | rest], [{x, y1} | acc])

  # vertical line (onwards)
  defp to_points([{{x1, y}, {x2, y}} | rest], acc) when x1 < x2,
    do: to_points([{{x1 + 1, y}, {x2, y}} | rest], [{x1, y} | acc])

  # vertical line (backwards)
  defp to_points([{{x1, y}, {x2, y}} | rest], acc),
    do: to_points([{{x1 - 1, y}, {x2, y}} | rest], [{x1, y} | acc])

  # diagonal line (onwards, upwards)
  defp to_points([{{x1, y1}, {x2, y2}} | rest], acc) when x1 < x2 and y1 < y2,
    do: to_points([{{x1 + 1, y1 + 1}, {x2, y2}} | rest], [{x1, y1} | acc])

  # diagonal line (onwards, downwards)
  defp to_points([{{x1, y1}, {x2, y2}} | rest], acc) when x1 < x2,
    do: to_points([{{x1 + 1, y1 - 1}, {x2, y2}} | rest], [{x1, y1} | acc])

  # diagonal line (backwards, upwards)
  defp to_points([{{x1, y1}, {x2, y2}} | rest], acc) when y1 < y2,
    do: to_points([{{x1 - 1, y1 + 1}, {x2, y2}} | rest], [{x1, y1} | acc])

  # diagonal line (backwards, downwards)
  defp to_points([{{x1, y1}, {x2, y2}} | rest], acc),
    do: to_points([{{x1 - 1, y1 - 1}, {x2, y2}} | rest], [{x1, y1} | acc])

  defp keep_duplicates([], acc), do: acc

  defp keep_duplicates([p | rest], acc) do
    if Enum.member?(rest, p) or Enum.member?(acc, p) do
      keep_duplicates(rest, [p | acc])
    else
      keep_duplicates(rest, acc)
    end
  end
end
