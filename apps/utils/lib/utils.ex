defmodule Utils do
  @moduledoc """
  Documentation for `Utils`.
  """
  @dialyzer {:nowarn_function, get_input: 1}
  @dialyzer {:nowarn_function, get_input: 2}

  @doc """
  Lazily reads an input file.
  """
  @spec get_input(String.t(), (String.t() -> term())) :: list(term())
  def get_input(filename, transform_fun \\ fn x -> x end) do
    Path.expand(filename)
    |> Path.absname()
    |> File.stream!([:read])
    |> Stream.map(&String.trim(&1))
    |> Stream.map(&transform_fun.(&1))
    |> Enum.to_list()
  end
end
