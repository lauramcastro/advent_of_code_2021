defmodule Advent14Test do
  use ExUnit.Case
  doctest Advent14

  test "answer to first challenge" do
    [input] = Utils.get_input("priv/input")
    pairs_as_map = input |> Advent14.split_in_overlapping_pairs() |> Map.new(fn p -> {p, 1} end)
    rules = Utils.get_input("priv/input_rules") |> Advent14.decode_rules()

    assert Advent14.polimerization_count(pairs_as_map, rules, 10, String.last(input)) == 2_899
  end

  test "answer to second challenge" do
    [input] = Utils.get_input("priv/input")
    pairs_as_map = input |> Advent14.split_in_overlapping_pairs() |> Map.new(fn p -> {p, 1} end)
    rules = Utils.get_input("priv/input_rules") |> Advent14.decode_rules()

    assert Advent14.polimerization_count(pairs_as_map, rules, 40, String.last(input)) ==
             3_528_317_079_545
  end
end
