defmodule Advent14 do
  @moduledoc """
  Documentation for `Advent14`.
  """

  require Logger

  @doc """
  Polimerization proccess final count.

  ## Examples

      iex> input = "NNCB"
      iex> pairs = input |> Advent14.split_in_overlapping_pairs()
      iex> pairs_as_map = pairs |> Map.new(fn p -> {p, 1} end)
      iex> rules = ["CH -> B",
      ...>          "HH -> N",
      ...>          "CB -> H",
      ...>          "NH -> C",
      ...>          "HB -> C",
      ...>          "HC -> B",
      ...>          "HN -> C",
      ...>          "NN -> C",
      ...>          "BH -> H",
      ...>          "NC -> B",
      ...>          "NB -> B",
      ...>          "BN -> B",
      ...>          "BB -> N",
      ...>          "BC -> B",
      ...>          "CC -> N",
      ...>          "CN -> C"] |> Advent14.decode_rules()
      iex> Advent14.polimerization_count(pairs_as_map, rules, 10, String.last(input))
      1_588
      iex> Advent14.polimerization_count(pairs_as_map, rules, 40, String.last(input))
      2_188_189_693_529

  """
  @spec polimerization_count(map(), map(), non_neg_integer(), String.t()) ::
          non_neg_integer()
  def polimerization_count(pairs, rules, n, last) do
    polymer = polimerization(pairs, rules, n)

    stats =
      Enum.reduce(
        polymer,
        %{},
        fn {k, v}, acc ->
          # we only need to count the first letter of each activated rule,
          # because the second letter will be the first of another rule
          [first, _] = String.codepoints(k)
          Map.update(acc, first, v, fn x -> x + v end)
        end
      )
      # we need to account for the last letter of the input,
      # since for that the above (see line 47) is not true!
      |> Map.update!(last, fn x -> x + 1 end)

    {_, most} = stats |> Enum.max_by(fn {_l, a} -> a end)
    {_, least} = stats |> Enum.min_by(fn {_l, a} -> a end)
    most - least
  end

  @doc """
  Polimerization proccess length.

  ## Examples

      iex> pairs = "NNCB" |> Advent14.split_in_overlapping_pairs()
      iex> pairs_as_map = pairs |> Map.new(fn p -> {p, 1} end)
      iex> rules = ["CH -> B",
      ...>          "HH -> N",
      ...>          "CB -> H",
      ...>          "NH -> C",
      ...>          "HB -> C",
      ...>          "HC -> B",
      ...>          "HN -> C",
      ...>          "NN -> C",
      ...>          "BH -> H",
      ...>          "NC -> B",
      ...>          "NB -> B",
      ...>          "BN -> B",
      ...>          "BB -> N",
      ...>          "BC -> B",
      ...>          "CC -> N",
      ...>          "CN -> C"] |> Advent14.decode_rules()
      iex> Advent14.polimerization_length(pairs_as_map, rules, 1)
      7
      iex> Advent14.polimerization_length(pairs_as_map, rules, 2)
      13
      iex> Advent14.polimerization_length(pairs_as_map, rules, 3)
      25
      iex> Advent14.polimerization_length(pairs_as_map, rules, 4)
      49
      iex> Advent14.polimerization_length(pairs_as_map, rules, 5)
      97
      iex> Advent14.polimerization_length(pairs_as_map, rules, 10)
      3_073

  """
  @spec polimerization_length(map(), map(), non_neg_integer()) :: non_neg_integer()
  def polimerization_length(pairs, rules, n),
    do:
      (polimerization(pairs, rules, n)
       |> Enum.reduce(0, fn {_k, v}, acc -> acc + v end)) + 1

  @doc """
  Polimerization proccess (n steps).

  ## Examples

      iex> pairs = "NNCB" |> Advent14.split_in_overlapping_pairs()
      iex> pairs_as_map = pairs |> Map.new(fn p -> {p, 1} end)
      iex> rules = ["CH -> B",
      ...>          "HH -> N",
      ...>          "CB -> H",
      ...>          "NH -> C",
      ...>          "HB -> C",
      ...>          "HC -> B",
      ...>          "HN -> C",
      ...>          "NN -> C",
      ...>          "BH -> H",
      ...>          "NC -> B",
      ...>          "NB -> B",
      ...>          "BN -> B",
      ...>          "BB -> N",
      ...>          "BC -> B",
      ...>          "CC -> N",
      ...>          "CN -> C"] |> Advent14.decode_rules()
      iex> Advent14.polimerization(pairs_as_map, rules, 1)
      %{"BC" => 1, "CH" => 1, "CN" => 1, "HB" => 1, "NB" => 1, "NC" => 1}

  """
  @spec polimerization(map(), map(), non_neg_integer()) :: map()
  def polimerization(pairs, _rules, 0), do: pairs

  def polimerization(pairs, rules, n),
    do: polimerization_step(pairs, rules) |> polimerization(rules, n - 1)

  @doc """
  Polimerization proccess (single step).

  ## Examples

      iex> pairs = "NNCB" |> Advent14.split_in_overlapping_pairs()
      iex> pairs_as_map = pairs |> Map.new(fn p -> {p, 1} end)
      iex> rules = ["CH -> B",
      ...>          "HH -> N",
      ...>          "CB -> H",
      ...>          "NH -> C",
      ...>          "HB -> C",
      ...>          "HC -> B",
      ...>          "HN -> C",
      ...>          "NN -> C",
      ...>          "BH -> H",
      ...>          "NC -> B",
      ...>          "NB -> B",
      ...>          "BN -> B",
      ...>          "BB -> N",
      ...>          "BC -> B",
      ...>          "CC -> N",
      ...>          "CN -> C"] |> Advent14.decode_rules()
      iex> Advent14.polimerization_step(pairs_as_map, rules)
      %{"BC" => 1, "CH" => 1, "CN" => 1, "NB" => 1, "NC" => 1, "HB" => 1}

  """
  @spec polimerization_step(map(), map()) :: map
  def polimerization_step(pairs, rules),
    do:
      Enum.reduce(pairs, %{}, fn {p, n}, acc ->
        # each step involves (1) applying the corresponding rule
        between = rules[p]
        [a, b] = String.codepoints(p)

        # and (2) accounting for its two new replacements
        acc
        |> Map.update(a <> between, n, fn x -> x + n end)
        |> Map.update(between <> b, n, fn x -> x + n end)
      end)

  @doc """
  Splits a string into pairs.

  ## Examples

      iex> Advent14.split_in_overlapping_pairs("NNCB")
      ["CB", "NC", "NN"]

  """
  @spec split_in_overlapping_pairs(String.t()) :: list(String.t())
  def split_in_overlapping_pairs(template) do
    String.codepoints(template) |> do_split_in_overlapping_pairs([])
  end

  defp do_split_in_overlapping_pairs([], acc), do: acc

  defp do_split_in_overlapping_pairs([_last], acc), do: do_split_in_overlapping_pairs([], acc)

  defp do_split_in_overlapping_pairs([a, b | rest], acc),
    do: do_split_in_overlapping_pairs([b | rest], [a <> b | acc])

  @doc """
  Turns a list of rules into a map.

  ## Examples

    iex> Advent14.decode_rules(["NN -> C",
    ...>                        "NC -> B",
    ...>                        "CB -> H"])
    %{"NN" => "C", "NC" => "B", "CB" => "H"}

  """
  @spec decode_rules(list(String.t())) :: map()
  def decode_rules(rulespec), do: do_decode_rules(rulespec, %{})

  defp do_decode_rules([], acc), do: acc

  defp do_decode_rules([r | more], acc) do
    [from, to] = String.split(r, " -> ")
    do_decode_rules(more, Map.put_new(acc, from, to))
  end
end
