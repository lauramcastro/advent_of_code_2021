defmodule Advent12Test do
  use ExUnit.Case
  doctest Advent12

  test "slightly larger example" do
    input =
      [
        "dc-end",
        "HN-start",
        "start-kj",
        "dc-start",
        "dc-HN",
        "LN-dc",
        "HN-end",
        "kj-sa",
        "kj-HN",
        "kj-dc"
      ]
      |> Enum.map(fn x -> String.split(x, "-") |> List.to_tuple() end)

    expected_output = [
      ["start", "HN", "dc", "HN", "end"],
      ["start", "HN", "dc", "HN", "kj", "HN", "end"],
      ["start", "HN", "dc", "end"],
      ["start", "HN", "dc", "kj", "HN", "end"],
      ["start", "HN", "end"],
      ["start", "HN", "kj", "HN", "dc", "HN", "end"],
      ["start", "HN", "kj", "HN", "dc", "end"],
      ["start", "HN", "kj", "HN", "end"],
      ["start", "HN", "kj", "dc", "HN", "end"],
      ["start", "HN", "kj", "dc", "end"],
      ["start", "dc", "HN", "end"],
      ["start", "dc", "HN", "kj", "HN", "end"],
      ["start", "dc", "end"],
      ["start", "dc", "kj", "HN", "end"],
      ["start", "kj", "HN", "dc", "HN", "end"],
      ["start", "kj", "HN", "dc", "end"],
      ["start", "kj", "HN", "end"],
      ["start", "kj", "dc", "HN", "end"],
      ["start", "kj", "dc", "end"]
    ]

    assert Advent12.paths(input) == expected_output
  end

  test "even larger example" do
    input =
      [
        "fs-end",
        "he-DX",
        "fs-he",
        "start-DX",
        "pj-DX",
        "end-zg",
        "zg-sl",
        "zg-pj",
        "pj-he",
        "RW-he",
        "fs-DX",
        "pj-RW",
        "zg-RW",
        "start-pj",
        "he-WI",
        "zg-he",
        "pj-fs",
        "start-RW"
      ]
      |> Enum.map(fn x -> String.split(x, "-") |> List.to_tuple() end)

    assert length(Advent12.paths(input)) == 226
  end

  test "answer to first challenge" do
    map = Utils.get_input("priv/input", fn x -> String.split(x, "-") |> List.to_tuple() end)

    assert length(Advent12.paths(map)) == 5_874
  end

  test "slightly larger example (one revisit to small cave allowed)" do
    input =
      [
        "dc-end",
        "HN-start",
        "start-kj",
        "dc-start",
        "dc-HN",
        "LN-dc",
        "HN-end",
        "kj-sa",
        "kj-HN",
        "kj-dc"
      ]
      |> Enum.map(fn x -> String.split(x, "-") |> List.to_tuple() end)

    assert length(Advent12.paths(input, :revisits_two_small_caves?)) == 103
  end

  test "even larger example (one revisit to small cave allowed)" do
    input =
      [
        "fs-end",
        "he-DX",
        "fs-he",
        "start-DX",
        "pj-DX",
        "end-zg",
        "zg-sl",
        "zg-pj",
        "pj-he",
        "RW-he",
        "fs-DX",
        "pj-RW",
        "zg-RW",
        "start-pj",
        "he-WI",
        "zg-he",
        "pj-fs",
        "start-RW"
      ]
      |> Enum.map(fn x -> String.split(x, "-") |> List.to_tuple() end)

    assert length(Advent12.paths(input, :revisits_two_small_caves?)) == 3_509
  end

  @tag :skip
  test "answer to second challenge" do
    map = Utils.get_input("priv/input", fn x -> String.split(x, "-") |> List.to_tuple() end)

    assert length(Advent12.paths(map, :revisits_two_small_caves?)) == 153_592
  end
end
