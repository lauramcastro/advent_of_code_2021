defmodule Advent12 do
  @moduledoc """
  Documentation for `Advent12`.
  """

  require Logger

  @doc """
  Finding paths.

  ## Examples

      iex> Advent12.paths([{"start","A"},
      ...>                 {"start","b"},
      ...>                 {"A","c"},
      ...>                 {"A","b"},
      ...>                 {"b","d"},
      ...>                 {"A","end"},
      ...>                 {"b","end"}])
      [["start","A","b","A","c","A","end"],
       ["start","A","b","A","end"],
       ["start","A","b","end"],
       ["start","A","c","A","b","A","end"],
       ["start","A","c","A","b","end"],
       ["start","A","c","A","end"],
       ["start","A","end"],
       ["start","b","A","c","A","end"],
       ["start","b","A","end"],
       ["start","b","end"]]

      iex> Advent12.paths([{"start","A"},
      ...>                 {"start","b"},
      ...>                 {"A","c"},
      ...>                 {"A","b"},
      ...>                 {"b","d"},
      ...>                 {"A","end"},
      ...>                 {"b","end"}], :revisits_two_small_caves?)
      [["start","A","b","A","b","A","c","A","end"],
      ["start","A","b","A","b","A","end"],
      ["start","A","b","A","b","end"],
      ["start","A","b","A","c","A","b","A","end"],
      ["start","A","b","A","c","A","b","end"],
      ["start","A","b","A","c","A","c","A","end"],
      ["start","A","b","A","c","A","end"],
      ["start","A","b","A","end"],
      ["start","A","b","d","b","A","c","A","end"],
      ["start","A","b","d","b","A","end"],
      ["start","A","b","d","b","end"],
      ["start","A","b","end"],
      ["start","A","c","A","b","A","b","A","end"],
      ["start","A","c","A","b","A","b","end"],
      ["start","A","c","A","b","A","c","A","end"],
      ["start","A","c","A","b","A","end"],
      ["start","A","c","A","b","d","b","A","end"],
      ["start","A","c","A","b","d","b","end"],
      ["start","A","c","A","b","end"],
      ["start","A","c","A","c","A","b","A","end"],
      ["start","A","c","A","c","A","b","end"],
      ["start","A","c","A","c","A","end"],
      ["start","A","c","A","end"],
      ["start","A","end"],
      ["start","b","A","b","A","c","A","end"],
      ["start","b","A","b","A","end"],
      ["start","b","A","b","end"],
      ["start","b","A","c","A","b","A","end"],
      ["start","b","A","c","A","b","end"],
      ["start","b","A","c","A","c","A","end"],
      ["start","b","A","c","A","end"],
      ["start","b","A","end"],
      ["start","b","d","b","A","c","A","end"],
      ["start","b","d","b","A","end"],
      ["start","b","d","b","end"],
      ["start","b","end"]]
  """
  @spec paths(list({String.t(), String.t()}), atom()) :: list(list(String.t()))
  def paths(map, revisits \\ :revisits_small_caves?) do
    step("start", map) |> do_step(map, revisits, []) |> Enum.sort()
  end

  defp do_step([], _, _, finished), do: finished

  defp do_step(alternatives, map, revisits, finished) do
    {more_finished, continuing} =
      Enum.reduce(alternatives, [], fn a, acc ->
        acc ++ (step(List.last(a), map) |> Enum.map(fn [_, d] -> a ++ [d] end))
      end)
      |> Enum.split_with(fn p -> List.last(p) == "end" end)

    Logger.debug(
      "So far found #{inspect(length(finished) + length(more_finished))} complete paths"
    )

    Enum.filter(continuing, fn p -> apply(__MODULE__, revisits, [p]) end)
    |> do_step(map, revisits, finished ++ more_finished)
  end

  defp step("end", _map), do: []

  defp step(position, map),
    do:
      Enum.flat_map(map, fn
        {a, b} when a == position and b != "start" -> [[a, b]]
        {a, b} when b == position and a != "start" -> [[b, a]]
        {_, _} -> []
      end)

  def revisits_small_caves?(p) do
    last = List.last(p)

    # either it is not a small cave, OR it has not been visited
    not is_small?(last) or
      not Enum.member?(List.delete(p, last), last)
  end

  def revisits_two_small_caves?(p) do
    last = List.last(p)

    # either it is not a small cave, OR it has not been visited (revisits_small_caves?)
    # OR, if it has been visited, there are no other small cave visited twice
    revisits_small_caves?(p) or
      no_small_cave_duplicates?(List.delete(p, last))
  end

  def is_small?(cave), do: cave == String.downcase(cave)

  def no_small_cave_duplicates?(l) do
    # we remove the "start" and the large caves
    small_caves = tl(l) |> Enum.filter(fn c -> is_small?(c) end)
    small_caves == Enum.uniq(small_caves)
  end
end
