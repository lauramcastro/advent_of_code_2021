defmodule Advent08 do
  @moduledoc """
  Documentation for `Advent08`.
  """

  @doc """
  Counting unique digits.

  In a seven-segment display, the number of digits used to represent:
  0 => 6
  1 => 2 [uniq]
  2 => 5
  3 => 5
  4 => 4 [uniq]
  5 => 5
  6 => 6
  7 => 3 [uniq]
  8 => 7 [uniq]
  9 => 6

  ## Examples

      iex> Advent08.count_unique(
      ...> ["be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe",
      ...>  "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc",
      ...>  "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg",
      ...>  "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb",
      ...>  "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea",
      ...>  "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb",
      ...>  "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe",
      ...>  "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef",
      ...>  "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb",
      ...>  "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"]
      ...> )
      26

  """
  @spec count_unique(list(String.t())) :: non_neg_integer()
  def count_unique(entries) do
    entries
    |> Enum.map(fn e -> String.split(e, " | ") end)
    |> Enum.map(fn [_pattern, output] -> output end)
    |> Enum.map(fn o -> String.split(o) end)
    |> Enum.map(fn digits ->
      Enum.map(digits, fn d -> String.length(d) end) |> unique_occurences(0)
    end)
    |> Enum.sum()
  end

  defp unique_occurences([], acc), do: acc

  defp unique_occurences([digit | rest], acc)
       when digit == 2 or digit == 3 or digit == 4 or digit == 7,
       do: unique_occurences(rest, acc + 1)

  defp unique_occurences([_digit | rest], acc), do: unique_occurences(rest, acc)

  @doc """
  Sum of decoded signals.

    ## Examples

      iex>  Advent08.decode_and_sum(
      ...> ["be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe",
      ...>  "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc",
      ...>  "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg",
      ...>  "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb",
      ...>  "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea",
      ...>  "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb",
      ...>  "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe",
      ...>  "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef",
      ...>  "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb",
      ...>  "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"]
      ...> )
      61229

  """
  @spec decode_and_sum(list(String.t())) :: non_neg_integer()
  def decode_and_sum(signals) do
    for(s <- signals, do: decode(s)) |> Enum.sum()
  end

  @doc """
  Decoding signals.

    ## Examples

      iex> Advent08.decode("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")
      5353

      iex> Advent08.decode("be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe")
      8394

      iex> Advent08.decode("edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc")
      9781

      iex> Advent08.decode("fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg")
      1197

      iex> Advent08.decode("fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb")
      9361

      iex> Advent08.decode("aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea")
      4873

      iex> Advent08.decode("fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb")
      8418

      iex> Advent08.decode("dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe")
      4548

      iex> Advent08.decode("bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef")
      1625

      iex> Advent08.decode("egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb")
      8717

      iex> Advent08.decode("gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce")
      4315

  """
  @spec decode(String.t()) :: non_neg_integer()
  def decode(signal) do
    [pattern, output] = String.split(signal, " | ")

    decode_pattern(pattern) |> decode_output(output) |> Enum.join() |> String.to_integer()
  end

  defp decode_output(keymap, output),
    do:
      for(
        o <- String.split(output),
        do: Map.get(keymap, String.codepoints(o) |> Enum.sort() |> Enum.join()) |> to_string
      )

  @doc """
  Decoding a signal.

    ## Examples

      iex> Advent08.decode_pattern("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab")
      %{"abcdeg" => 0, "ab" => 1, "acdfg" => 2, "abcdf" => 3, "abef" => 4,
        "bcdef" => 5, "bcdefg" => 6, "abd" => 7, "abcdefg" => 8, "abcdef" => 9}
    
  """
  def decode_pattern(pattern) do
    {done, pending} =
      pattern
      |> String.split()
      |> Enum.map(fn p ->
        {String.length(p), String.codepoints(p) |> Enum.sort() |> Enum.join()} # sort each no. so we can more easily decode later on
      end)
      |> Enum.reduce({%{}, %{}}, fn {l, digit}, acc -> decode_digit(l, digit, acc) end) # straigth away decode unique digits

    # we deduce no.3 because it contains no.1
    [one] = for {key, 1} <- done, do: key
    three = Enum.find(pending[5], fn digit -> contains_all(digit, one) end)
    pending = Map.update!(pending, 5, fn existing -> existing -- [three] end)
    done = Map.put(done, three, 3)
    # we deduce no.5 because it contains (no.4 - no.1), and the one which does not is no.2
    [four] = for {key, 4} <- done, do: key
    five = Enum.find(pending[5], fn digit -> contains_all(digit, subtract(one, four)) end)
    pending = Map.update!(pending, 5, fn existing -> existing -- [five] end)
    [two] = pending[5]
    done = Map.put(done, five, 5) |> Map.put(two, 2)
    # we deduce no.6 because it does not contain no.1
    six = Enum.find(pending[6], fn digit -> not contains_all(digit, one) end)
    pending = Map.update!(pending, 6, fn existing -> existing -- [six] end)
    done = Map.put(done, six, 6)
    # we deduce no.9 because it contains no.4, and the one which does not is no.0
    nine = Enum.find(pending[6], fn digit -> contains_all(digit, four) end)
    pending = Map.update!(pending, 6, fn existing -> existing -- [nine] end)
    [zero] = pending[6]

    Map.put(done, nine, 9) |> Map.put(zero, 0)
  end

  defp decode_digit(2, digit, {map, pending}), do: {Map.put(map, digit, 1), pending}
  defp decode_digit(4, digit, {map, pending}), do: {Map.put(map, digit, 4), pending}
  defp decode_digit(3, digit, {map, pending}), do: {Map.put(map, digit, 7), pending}
  defp decode_digit(7, digit, {map, pending}), do: {Map.put(map, digit, 8), pending}

  defp decode_digit(n, digit, {map, pending}),
    do: {map, Map.update(pending, n, [digit], fn ds -> [digit | ds] end)}

  defp contains_all(str1, str2),
    do: Enum.all?(String.codepoints(str2), fn c -> String.contains?(str1, c) end)

  defp subtract(str1, str2),
    do: (String.codepoints(str2) -- String.codepoints(str1)) |> Enum.join()
end
