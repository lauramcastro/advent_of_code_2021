defmodule Advent08Test do
  use ExUnit.Case
  doctest Advent08

  test "answer to first challenge" do
    entries = Utils.get_input("priv/input")
    assert Advent08.count_unique(entries) == 530
  end

  test "answer to second challenge" do
    entries = Utils.get_input("priv/input")
    assert Advent08.decode_and_sum(entries) == 1_051_087
  end
end
