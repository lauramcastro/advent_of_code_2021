defmodule Advent11 do
  @moduledoc """
  Documentation for `Advent11`.
  """

  @zeroes Matrex.zeros(10)

  require Logger

  @doc """
  Conway game with octopuses! (n steps)

  ## Examples
      iex> {m, _} = Matrex.new([[1,1,1,1,1],
      ...>                     [1,9,9,9,1],
      ...>                     [1,9,1,9,1],
      ...>                     [1,9,9,9,1],
      ...>                     [1,1,1,1,1]]) |> Advent11.steps(2)
      ...> m == Matrex.new([[4,5,6,5,4],
      ...>                  [5,1,1,1,5],
      ...>                  [6,1,1,1,6],
      ...>                  [5,1,1,1,5],
      ...>                  [4,5,6,5,4]])
      true

  """
  @spec steps(Matrex.t(), non_neg_integer()) :: {Matrex.t(), non_neg_integer()}
  def steps(energies, n) when n > 0, do: do_steps(energies, n, 0)

  defp do_steps(energies, 0, acc), do: {energies, acc}

  defp do_steps(energies, n, acc) do
    {new_energies, flashes} = step(energies)
    do_steps(new_energies, n - 1, flashes + acc)
  end

  @doc """
  Conway game with octopuses! (evolves until it is all zeroes)
  """
  @spec steps_until_synchronized(Matrex.t()) :: non_neg_integer()
  def steps_until_synchronized(energies), do: do_sync_steps(energies, false, 0)

  defp do_sync_steps(_energies, true, acc), do: acc

  defp do_sync_steps(energies, false, acc) do
    {new_energies, _flashes} = step(energies)
    do_sync_steps(new_energies, new_energies == @zeroes, acc + 1)
  end

  @doc """
  Conway game with octopuses! (one step)

  ## Examples

      iex> output = Matrex.new([[1,1,1,1,1],
      ...>                      [1,9,9,9,1],
      ...>                      [1,9,1,9,1],
      ...>                      [1,9,9,9,1],
      ...>                      [1,1,1,1,1]]) |> Advent11.step()
      ...> output == { Matrex.new([[3,4,5,4,3],
      ...>                         [4,0,0,0,4],
      ...>                         [5,0,0,0,5],
      ...>                         [4,0,0,0,4],
      ...>                         [3,4,5,4,3]]), 9 }
      true

  """
  @spec step(Matrex.t()) :: {Matrex.t(), non_neg_integer()}
  def step(energies) do
    {rows, columns} = Matrex.size(energies)
    all_indexes = for(r <- 1..rows, do: for(c <- 1..columns, do: {r, c})) |> List.flatten()

    {evolved_energies, num_flashes} =
      increase_one(energies, all_indexes) |> flash({rows, columns}, all_indexes, [])

    {evolved_energies |> normalize(), num_flashes}
  end

  defp increase_one(m, []), do: m

  defp increase_one(m, [{r, c} | rest]) do
    Matrex.update(m, r, c, fn x -> x + 1 end) |> increase_one(rest)
  end

  defp normalize(m),
    do:
      Matrex.apply(m, fn
        value when value > 9.0 -> 0.0
        value -> value
      end)

  defp flash(m, _size, [], flashed), do: {m, Enum.count(flashed)}

  defp flash(m, size, indexes, already_flashed) do
    flashed =
      Enum.reduce(
        indexes,
        [],
        fn {r, c}, acc ->
          # first time it flashes
          if Matrex.at(m, r, c) > 9.0 and not Enum.member?(already_flashed, {r, c}) do
            [{r, c} | acc]
          else
            acc
          end
        end
      )

    Logger.debug("Those which flashed where #{inspect(flashed)}")

    neighbours =
      Enum.reduce(
        flashed,
        [],
        fn p, acc -> MyMatrex.neighbours(size, p) ++ acc end
      )

    Logger.debug("Their neighbours are #{inspect(neighbours)}")

    increase_one(m, neighbours) |> flash(size, Enum.uniq(neighbours), flashed ++ already_flashed)
  end
end
