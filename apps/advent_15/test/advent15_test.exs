defmodule Advent15Test do
  use ExUnit.Case
  doctest Advent15

  test "answer to first challenge" do
    map =
      Utils.get_input("priv/input", fn x ->
        String.codepoints(x) |> Enum.map(fn d -> String.to_integer(d) end)
      end)
      |> Matrex.new()

    assert Advent15.weight_of_lowest_weight_path(map) == 583
  end

  @tag :skip
  test "answer to second challenge" do
    map =
      Utils.get_input("priv/input", fn x ->
        String.codepoints(x) |> Enum.map(fn d -> String.to_integer(d) end)
      end)
      |> Advent15.multiply()

    # but this is the wrong answer: it should be higher :(
    assert Advent15.weight_of_lowest_weight_path(map) == 1_464
  end
end
