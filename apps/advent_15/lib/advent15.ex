defmodule Advent15 do
  @moduledoc """
  Documentation for `Advent15`.
  """

  require Logger

  @doc """
  Lowest-weight (risk) path through a matrix using Graph.
  This is classically solved by Dijkstra's algorithm (https://en.wikipedia.org/wiki/Dijkstra's_algorithm).

  The assumption we only go forward and downwards DOES WORK on the final input!

  ## Examples

      iex> Matrex.new([[1,1,6,3,7,5,1,7,4,2],
      ...>             [1,3,8,1,3,7,3,6,7,2],
      ...>             [2,1,3,6,5,1,1,3,2,8],
      ...>             [3,6,9,4,9,3,1,5,6,9],
      ...>             [7,4,6,3,4,1,7,1,1,1],
      ...>             [1,3,1,9,1,2,8,1,3,7],
      ...>             [1,3,5,9,9,1,2,4,2,1],
      ...>             [3,1,2,5,4,2,1,6,3,9],
      ...>             [1,2,9,3,1,3,8,5,2,1],
      ...>             [2,3,1,1,9,4,4,5,8,1]]) |>
      ...> Advent15.lowest_weight_path()
      [{1,1},{2,1},{3,1},{3,2},{3,3},{3,4},{3,5},{3,6},{3,7},{4,7},{4,8},{5,8},{6,8},{6,9},{7,9},{8,9},{9,9},{9,10},{10,10}]

  """
  @spec lowest_weight_path(Matrex.t()) :: list({non_neg_integer(), non_neg_integer()})
  def lowest_weight_path(map) do
    {max_r, max_c} = map[:size]

    vertexes =
      for r <- Enum.to_list(1..max_r),
          do: for(c <- Enum.to_list(1..max_c), do: {r, c})

    vertexes
    |> List.flatten()
    |> Enum.reduce(
      Graph.new(),
      fn v, g ->
        MyMatrex.neighbours({max_r, max_c}, v, false)
        |> Enum.reduce(g, fn nb = {nbr, nbc}, g ->
          Graph.add_edge(g, Graph.Edge.new(v, nb, weight: map[nbr][nbc]))
        end)
      end
    )
    |> Graph.dijkstra({1, 1}, {max_r, max_c})
  end

  @doc """
  Weight of lowest-weight (risk) path through a matrix using Graph.

  ## Examples

      iex> Matrex.new([[1,1,6,3,7,5,1,7,4,2],
      ...>             [1,3,8,1,3,7,3,6,7,2],
      ...>             [2,1,3,6,5,1,1,3,2,8],
      ...>             [3,6,9,4,9,3,1,5,6,9],
      ...>             [7,4,6,3,4,1,7,1,1,1],
      ...>             [1,3,1,9,1,2,8,1,3,7],
      ...>             [1,3,5,9,9,1,2,4,2,1],
      ...>             [3,1,2,5,4,2,1,6,3,9],
      ...>             [1,2,9,3,1,3,8,5,2,1],
      ...>             [2,3,1,1,9,4,4,5,8,1]]) |>
      ...> Advent15.weight_of_lowest_weight_path()
      40

      iex> Advent15.multiply([[1,1,6,3,7,5,1,7,4,2],
      ...>                 [1,3,8,1,3,7,3,6,7,2],
      ...>                 [2,1,3,6,5,1,1,3,2,8],
      ...>                 [3,6,9,4,9,3,1,5,6,9],
      ...>                 [7,4,6,3,4,1,7,1,1,1],
      ...>                 [1,3,1,9,1,2,8,1,3,7],
      ...>                 [1,3,5,9,9,1,2,4,2,1],
      ...>                 [3,1,2,5,4,2,1,6,3,9],
      ...>                 [1,2,9,3,1,3,8,5,2,1],
      ...>                 [2,3,1,1,9,4,4,5,8,1]]) |>
      ...> Advent15.weight_of_lowest_weight_path()
      315

  """
  @spec weight_of_lowest_weight_path(Matrex.t()) :: non_neg_integer()
  def weight_of_lowest_weight_path(map) do
    weight =
      lowest_weight_path(map)
      |> Enum.reduce(0, fn {r, c}, acc -> map[r][c] + acc end)

    trunc(weight - map[1][1])
  end

  @doc """
  Multiplies a matrix 5 times its size in the two dimensions

  ## Examples

      iex> Advent15.multiply([[8]]) |> Matrex.to_list_of_lists
      [[8.0, 9.0, 1.0, 2.0, 3.0],
       [9.0, 1.0, 2.0, 3.0, 4.0],
       [1.0, 2.0, 3.0, 4.0, 5.0],
       [2.0, 3.0, 4.0, 5.0, 6.0],
       [3.0, 4.0, 5.0, 6.0, 7.0]
      ]

  """
  @spec multiply(list(list(non_neg_integer()))) :: Matrex.t()
  def multiply(data) do
    orig = Matrex.new(data)
    {orig_rows, orig_cols} = orig[:size]
    {dest_rows, dest_cols} = {orig_rows * 5, orig_cols * 5}
    dest = Matrex.fill(dest_rows, dest_cols, 0)

    indexes =
      for fr <- Enum.to_list(1..orig_rows),
          do:
            for(
              fc <- Enum.to_list(1..orig_cols),
              do:
                for(
                  tr <- Enum.to_list(fr..dest_rows//orig_rows),
                  do:
                    for(
                      tc <- Enum.to_list(fc..dest_cols//orig_cols),
                      do: {fr, fc, tr, tc, div(tr - 1, orig_rows) + div(tc - 1, orig_cols)}
                    )
                )
            )

    List.flatten(indexes) |> apply_indexes(orig, dest)
  end

  defp apply_indexes([], _orig, dest), do: dest

  defp apply_indexes([{from_r, from_c, to_r, to_c, increase} | more], orig, dest) do
    value = trunc_over_nine(Matrex.at(orig, from_r, from_c) + increase)
    apply_indexes(more, orig, Matrex.set(dest, to_r, to_c, value))
  end

  defp trunc_over_nine(n) when n <= 9, do: n
  defp trunc_over_nine(n), do: Integer.mod(trunc(n), 10) + 1
end
