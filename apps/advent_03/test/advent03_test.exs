defmodule Advent03Test do
  use ExUnit.Case
  doctest Advent03

  test "answer to first challenge" do
    diagnostics = Utils.get_input("priv/input", fn x -> String.to_charlist(x) end)
    gamma_rate = Advent03.gamma_rate(diagnostics)
    epsilon_rate = Advent03.epsilon_rate(gamma_rate)

    assert Advent03.power_consumption(gamma_rate, epsilon_rate) == 4_147_524
  end

  test "answer to second challenge" do
    diagnostics = Utils.get_input("priv/input", fn x -> String.to_charlist(x) end)
    oxygen_rate = Advent03.oxygen_generation_rate(diagnostics)
    co2_rate = Advent03.co2_scrubbing_rate(diagnostics)

    assert Advent03.power_consumption(oxygen_rate, co2_rate) == 3_570_354
  end
end
