defmodule Advent03 do
  @moduledoc """
  Documentation for `Advent03`.
  """

  @doc """
  The gamma rate can be determined by finding the most common bit for each position.

  ## Examples

      iex> Advent03.gamma_rate(['00100',
      ...>                      '11110',
      ...>                      '10110',
      ...>                      '10111',
      ...>                      '10101',
      ...>                      '01111',
      ...>                      '00111',
      ...>                      '11100',
      ...>                      '10000',
      ...>                      '11001',
      ...>                      '00010',
      ...>                      '01010'])
      "10110"

      iex> Advent03.gamma_rate(['0010001',
      ...>                      '1111001',
      ...>                      '1011001',
      ...>                      '1011101',
      ...>                      '1010101',
      ...>                      '0111101',
      ...>                      '0011101',
      ...>                      '1110001',
      ...>                      '1000001',
      ...>                      '1100101',
      ...>                      '0001001',
      ...>                      '0101001'])
      "1011001"
  """
  @spec gamma_rate(list(charlist())) :: String.t()
  def gamma_rate(diagnostics) do
    [sample | _rest] = diagnostics
    {length, zero_counts} = count_bits(diagnostics, 0, for(_ <- 1..length(sample), do: 0))
    occurrences = div(length, 2)

    Enum.map(
      zero_counts,
      fn
        b when b > occurrences -> ?0
        _b -> ?1
      end
    )
    |> to_string
  end

  defp count_bits([], processed, rate), do: {processed, rate}

  defp count_bits([diagnostic | more_diagnostics], processed, rate) do
    count_bits(more_diagnostics, processed + 1, accumulate(diagnostic, rate))
  end

  defp accumulate(diagnostic, rate) do
    Enum.zip(diagnostic, rate)
    |> Enum.map(fn
      {?0, n} -> n + 1
      {?1, n} -> n
    end)
  end

  @doc """
  The epsilon rate is the complementary of the gamma rate.

  ## Examples

      iex> Advent03.epsilon_rate("10110")
      "01001"

  """
  @spec epsilon_rate(String.t()) :: String.t()
  def epsilon_rate(gamma_rate) do
    String.to_charlist(gamma_rate)
    |> Enum.map(fn
      ?0 -> ?1
      ?1 -> ?0
    end)
    |> to_string
  end

  @doc """
  Power consumption is the epsilon rate multiplied by the gamma rate.

  ## Examples

      iex> Advent03.power_consumption("10110", "01001")
      198

  """
  @spec power_consumption(String.t(), String.t()) :: integer()
  def power_consumption(gamma_rate, epsilon_rate) do
    String.to_integer(gamma_rate, 2) * String.to_integer(epsilon_rate, 2)
  end

  @doc """
  Oxigen generation rate is obtained filtering by most common bit.

  ## Examples

      iex> Advent03.oxygen_generation_rate(['00100',
      ...>                                  '11110',
      ...>                                  '10110',
      ...>                                  '10111',
      ...>                                  '10101',
      ...>                                  '01111',
      ...>                                  '00111',
      ...>                                  '11100',
      ...>                                  '10000',
      ...>                                  '11001',
      ...>                                  '00010',
      ...>                                  '01010'])
      "10111"

  """
  @spec oxygen_generation_rate(list(charlist())) :: String.t()
  def oxygen_generation_rate(diagnostics) do
    Enum.zip(diagnostics, diagnostics) |> filter(:greater) |> to_string
  end

  defp filter([{_, original}], _criteria), do: original

  defp filter(diagnostics, criteria) do
    {start_with_zero, start_with_one} =
      Enum.split_with(diagnostics, fn {[c | _rest], _} -> c == ?0 end)

    choose(
      criteria,
      {length(start_with_zero), start_with_zero},
      {length(start_with_one), start_with_one}
    )
    |> Enum.map(fn {[_c | rest], original} -> {rest, original} end)
    |> filter(criteria)
  end

  defp choose(:greater, {a, value_a}, {b, _}) when a > b, do: value_a
  defp choose(:greater, _, {_, value_b}), do: value_b
  defp choose(:lesser, {a, value_a}, {b, _value_b}) when a <= b, do: value_a
  defp choose(:lesser, _, {_, value_b}), do: value_b

  @doc """
  CO2 scrubbing rate is obtained filtering by least common bit.

  ## Examples

      iex> Advent03.co2_scrubbing_rate(['00100',
      ...>                              '11110',
      ...>                              '10110',
      ...>                              '10111',
      ...>                              '10101',
      ...>                              '01111',
      ...>                              '00111',
      ...>                              '11100',
      ...>                              '10000',
      ...>                              '11001',
      ...>                              '00010',
      ...>                              '01010'])
      "01010"

  """
  @spec co2_scrubbing_rate(list(charlist())) :: String.t()
  def co2_scrubbing_rate(diagnostics) do
    Enum.zip(diagnostics, diagnostics) |> filter(:lesser) |> to_string
  end
end
