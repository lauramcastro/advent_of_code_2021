defmodule Advent13 do
  @moduledoc """
  Documentation for `Advent13`.
  """

  require Logger

  @doc """
  Origami!

  """
  @spec translate_origami(list({non_neg_integer(), non_neg_integer()}), list(String.t())) :: :ok
  def translate_origami(paper, fold) do
    points = origami_steps(paper, fold)

    {max_x, max_y} = Enum.max(points)

    Enum.reduce(points, Matrex.zeros(max_y + 1, max_x + 1), fn {x, y}, acc ->
      # to {row, column}
      Matrex.set(acc, y + 1, x + 1, 1)
    end)
    # this is meant to be visually inspected!
    |> Matrex.heatmap(:mono8)

    :ok
  end

  @doc """
  Origami!

  ## Examples

      iex> Advent13.origami_count([{6,10},
      ...>                         {0,14},
      ...>                         {9,10},
      ...>                         {0,3},
      ...>                         {10,4},
      ...>                         {4,11},
      ...>                         {6,0},
      ...>                         {6,12},
      ...>                         {4,1},
      ...>                         {0,13},
      ...>                         {10,12},
      ...>                         {3,4},
      ...>                         {3,0},
      ...>                         {8,4},
      ...>                         {1,10},
      ...>                         {2,14},
      ...>                         {8,10},
      ...>                         {9,0}], ["fold along y=7"])
      17

      iex> Advent13.origami_count([{0, 0},
      ...>                         {0, 1},
      ...>                         {0, 3},
      ...>                         {1, 4},
      ...>                         {2, 0},
      ...>                         {3, 0},
      ...>                         {3, 4},
      ...>                         {4, 1},
      ...>                         {4, 3},
      ...>                         {6, 0},
      ...>                         {6, 2},
      ...>                         {6, 4},
      ...>                         {8, 4},
      ...>                         {9, 0},
      ...>                         {9, 4},
      ...>                         {10, 2},
      ...>                         {10, 4}], ["fold along x=5"])
      16

  """
  @spec origami_count(list({non_neg_integer(), non_neg_integer()}), list(String.t())) ::
          non_neg_integer()
  def origami_count(paper, fold), do: origami_steps(paper, fold) |> Enum.count()

  @doc """
  Origami!

  ## Examples

      iex> Advent13.origami([{6,10},
      ...>                   {0,14},
      ...>                   {9,10},
      ...>                   {0,3},
      ...>                   {10,4},
      ...>                   {4,11},
      ...>                   {6,0},
      ...>                   {6,12},
      ...>                   {4,1},
      ...>                   {0,13},
      ...>                   {10,12},
      ...>                   {3,4},
      ...>                   {3,0},
      ...>                   {8,4},
      ...>                   {1,10},
      ...>                   {2,14},
      ...>                   {8,10},
      ...>                   {9,0}], ["fold along y=7"])
      [{0, 0},{0, 1},{0, 3},{1, 4},{2, 0},{3, 0},{3, 4},{4, 1},{4, 3},{6, 0},{6, 2},{6, 4},{8, 4},{9, 0},{9, 4},{10, 2},{10, 4}]

      iex> Advent13.origami([{0, 0},
      ...>                   {0, 1},
      ...>                   {0, 3},
      ...>                   {1, 4},
      ...>                   {2, 0},
      ...>                   {3, 0},
      ...>                   {3, 4},
      ...>                   {4, 1},
      ...>                   {4, 3},
      ...>                   {6, 0},
      ...>                   {6, 2},
      ...>                   {6, 4},
      ...>                   {8, 4},
      ...>                   {9, 0},
      ...>                   {9, 4},
      ...>                   {10, 2},
      ...>                   {10, 4}], ["fold along x=5"])
      [{0, 0},{0, 1},{0, 2},{0, 3},{0, 4},{1, 0},{1, 4},{2, 0},{2, 4},{3, 0},{3, 4},{4, 0},{4, 1},{4, 2},{4, 3},{4, 4}]

  """
  @spec origami(list({non_neg_integer(), non_neg_integer()}), list(String.t())) ::
          [{non_neg_integer(), non_neg_integer()}]
  def origami(paper, fold), do: origami_steps(paper, fold) |> Enum.sort()

  defp origami_steps(paper, []), do: paper

  defp origami_steps(paper, [fold | rest]) do
    {how, where} = process_fold(fold)
    do_origami(how, where, paper) |> Enum.uniq() |> origami_steps(rest)
  end

  defp do_origami(direction, where, paper) do
    {points_to_fold, rest_of_points} =
      Enum.split_with(paper, fn p -> needs_folding?(p, direction, where) end)

    Enum.reduce(points_to_fold, rest_of_points, fn p, acc ->
      new_p = fold(direction, where, p)
      # Logger.debug("Folding #{inspect(p)} into #{inspect(new_p)}")
      [new_p | acc]
    end)
  end

  defp needs_folding?({_x, y}, :up, where), do: y > where
  defp needs_folding?({x, _y}, :left, where), do: x > where

  defp fold(:up, where, {x, y}), do: {x, 2 * where - y}
  defp fold(:left, where, {x, y}), do: {2 * where - x, y}

  defp process_fold(fold) do
    ["fold", "along", data] = String.split(fold)
    {how, where} = String.split(data, "=") |> List.to_tuple()
    {direction(how), String.to_integer(where)}
  end

  defp direction("y"), do: :up
  defp direction("x"), do: :left
end
