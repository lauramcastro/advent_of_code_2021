defmodule Advent13Test do
  use ExUnit.Case
  doctest Advent13

  require Logger

  test "answer to first challenge" do
    paper =
      Utils.get_input("priv/input", fn x ->
        [x, y] = String.split(x, ",")
        {String.to_integer(x), String.to_integer(y)}
      end)

    fold = Utils.get_input("priv/input_folds")

    assert Advent13.origami_count(paper, [hd(fold)]) == 763
  end

  test "answer to second challenge" do
    paper =
      Utils.get_input("priv/input", fn x ->
        [x, y] = String.split(x, ",")
        {String.to_integer(x), String.to_integer(y)}
      end)

    fold = Utils.get_input("priv/input_folds")

    # was: RHALRCRA
    assert Advent13.translate_origami(paper, fold) == :ok
  end
end
