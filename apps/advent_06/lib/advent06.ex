defmodule Advent06 do
  @moduledoc """
  Documentation for `Advent06`.
  """

  require Logger

  @doc """
  Monitor the life of a colony of lanternfishes.

  ## Examples

      iex> Advent06.lanternfishes([3,4,3,1,2], 80)
      5934

  """
  @spec lanternfishes(list(non_neg_integer()), non_neg_integer()) :: non_neg_integer()
  def lanternfishes(colony_spec, days) do
    colony = for(d <- colony_spec, do: Lanternfish.start_link(d)) |> monitor_colony(days)

    result = colony |> length
    for f <- colony, do: Lanternfish.stop(f)
    result
  end

  defp monitor_colony(fishes, 0), do: fishes

  defp monitor_colony(fishes, n) do
    Logger.debug("New day (#{inspect(n)} to go), colony size is #{inspect(length(fishes))}")

    new_fishes =
      for(f <- fishes, do: Lanternfish.day(f))
      |> Enum.reduce([], fn fs, acc -> acc ++ fs end)

    monitor_colony(fishes ++ new_fishes, n - 1)
  end

  @doc """
  Calculate the life of a colony of lanternfishes.

  ## Examples

      iex> Advent06.calculate_lanternfishes([3,4,3,1,2], 1)
      5

      iex> Advent06.calculate_lanternfishes([3,4,3,1,2], 2)
      6

      iex> Advent06.calculate_lanternfishes([3,4,3,1,2], 3)
      7

      iex> Advent06.calculate_lanternfishes([3,4,3,1,2], 4)
      9

      iex> Advent06.calculate_lanternfishes([3,4,3,1,2], 9)
      11

      iex> Advent06.calculate_lanternfishes([3,4,3,1,2], 18)
      26

      iex> Advent06.calculate_lanternfishes([3,4,3,1,2], 80)
      5934

      iex> Advent06.calculate_lanternfishes([3,4,3,1,2], 256)
      26984457539

  """
  @spec calculate_lanternfishes(list(String.t()), non_neg_integer()) :: non_neg_integer()
  def calculate_lanternfishes(colony_spec, days) do
    classify(colony_spec, %{}) |> calculate_colony(days) |> Map.values() |> Enum.sum()
  end

  defp classify([], acc), do: acc

  defp classify([k | rest], acc) do
    {_, new_acc} =
      Map.get_and_update(acc, k, fn
        nil -> {nil, 1}
        v -> {v, v + 1}
      end)

    classify(rest, new_acc)
  end

  defp calculate_colony(colony, 0), do: colony

  defp calculate_colony(colony, n),
    do:
      calculate_colony(
        %{
          0 => Map.get(colony, 1, 0),
          1 => Map.get(colony, 2, 0),
          2 => Map.get(colony, 3, 0),
          3 => Map.get(colony, 4, 0),
          4 => Map.get(colony, 5, 0),
          5 => Map.get(colony, 6, 0),
          6 => Map.get(colony, 7, 0) + Map.get(colony, 0, 0),
          7 => Map.get(colony, 8, 0),
          8 => Map.get(colony, 0, 0)
        },
        n - 1
      )
end
