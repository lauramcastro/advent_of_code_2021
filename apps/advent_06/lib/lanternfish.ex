defmodule Lanternfish do
  @moduledoc """
  Defines a lanternfish using a GenServer.
  """
  use GenServer
  require Logger

  @internal_timer 6

  # API

  @doc """
  Spawns a new fish.
  """
  @spec start_link(non_neg_integer()) :: pid()
  def start_link(days_to_spawn) do
    {:ok, f} = GenServer.start_link(__MODULE__, days_to_spawn)
    f
  end

  @doc """
  Lets the fish know a day has passed.
  As a reply, the fish may return a new one in case it has spawned one.
  """
  @spec day(pid()) :: list(pid())
  def day(p) do
    GenServer.call(p, :tick)
  end

  @doc """
  Releases the fish.
  """
  @spec stop(pid()) :: :ok
  def stop(p) do
    GenServer.stop(p)
  end

  # Callbacks

  @impl true
  def init(timer) do
    {:ok, timer}
  end

  @impl true
  def handle_call(:tick, _from, 0) do
    p = Lanternfish.start_link(@internal_timer + 2)

    # Logger.debug("Fish #{inspect(self())} spawns new fish #{inspect(p)}, new timer is #{@internal_timer}")
    {:reply, [p], @internal_timer}
  end

  def handle_call(:tick, _from, timer) do
    # Logger.debug("Fish #{inspect(self())} updates timer to #{timer - 1}")
    {:reply, [], timer - 1}
  end
end
