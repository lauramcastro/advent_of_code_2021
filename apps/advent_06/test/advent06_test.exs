defmodule Advent06Test do
  use ExUnit.Case
  doctest Advent06

  # in order to run this test the process limit of the VM must be increased
  # iex --erl "+P 1000000" mix test --trace
  @tag :skip
  test "answer to first challenge" do
    [numbers] = Utils.get_input("priv/input")
    numbers = String.split(numbers, ",") |> Enum.map(&String.to_integer(&1))
    assert Advent06.lanternfishes(numbers, 80) == 371_379
  end

  test "answer to first challenge (no processes)" do
    [numbers] = Utils.get_input("priv/input")
    numbers = String.split(numbers, ",") |> Enum.map(&String.to_integer(&1))
    assert Advent06.calculate_lanternfishes(numbers, 80) == 371_379
  end

  test "answer to second challenge (no processes)" do
    [numbers] = Utils.get_input("priv/input")
    numbers = String.split(numbers, ",") |> Enum.map(&String.to_integer(&1))
    assert Advent06.calculate_lanternfishes(numbers, 256) == 1_674_303_997_472
  end
end
