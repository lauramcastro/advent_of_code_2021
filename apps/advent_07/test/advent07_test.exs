defmodule Advent07Test do
  use ExUnit.Case
  doctest Advent07

  test "answer to first challenge" do
    [numbers] = Utils.get_input("priv/input")
    numbers = String.split(numbers, ",") |> Enum.map(&String.to_integer(&1))
    assert Advent07.optimal_aligment(numbers) == {:ok, 336_701}
  end

  test "answer to second challenge" do
    [numbers] = Utils.get_input("priv/input")
    numbers = String.split(numbers, ",") |> Enum.map(&String.to_integer(&1))
    assert Advent07.optimal_aligment(numbers, :non_constant) == {:ok, 95_167_302}
  end
end
