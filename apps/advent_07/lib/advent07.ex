defmodule Advent07 do
  @moduledoc """
  Documentation for `Advent07`.
  """

  @doc """
  Optimal crab alignment looks like distance to average, but it is not. However, it is a place to start.

  ## Examples

      iex> Advent07.optimal_aligment([16,1,2,0,4,2,7,1,2,14])
      {:ok, 37}

      iex> Advent07.optimal_aligment([16,1,2,0,4,2,7,1,2,14], :non_constant)
      {:ok, 168}
      
  """
  @spec optimal_aligment(list(non_neg_integer()), :constant | :non_constant) :: integer()
  def optimal_aligment(positions, mode \\ :constant) do
    Enum.min(positions)..Enum.max(positions)
    |> Enum.to_list()
    |> Task.async_stream(fn i -> distances_to(i, positions, mode) end)
    |> Enum.reduce(fn
      {:ok, val}, {:ok, acc} when val < acc -> {:ok, val}
      {:ok, _}, acc -> acc
    end)
  end

  defp distances_to(y, positions, :constant),
    do: Enum.map(positions, fn x -> abs(x - y) end) |> Enum.sum()

  defp distances_to(y, positions, :non_constant),
    do:
      Enum.map(positions, fn x -> 0..abs(x - y) |> Enum.to_list() |> Enum.sum() end) |> Enum.sum()
end
