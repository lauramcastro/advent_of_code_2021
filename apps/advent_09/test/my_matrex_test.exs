defmodule MyMatrexTest do
  use ExUnit.Case

  test "corners have three neighbours" do
    assert MyMatrex.neighbours({5, 5}, {1, 1}) == [{1, 2}, {2, 1}, {2, 2}]
    assert MyMatrex.neighbours({5, 5}, {1, 5}) == [{1, 4}, {2, 4}, {2, 5}]
    assert MyMatrex.neighbours({5, 5}, {5, 1}) == [{4, 1}, {4, 2}, {5, 2}]
    assert MyMatrex.neighbours({5, 5}, {5, 5}) == [{4, 4}, {4, 5}, {5, 4}]
  end

  test "elements in first/last row/column which are not corners have five neighbours" do
    # first row
    assert MyMatrex.neighbours({5, 5}, {1, 2}) == [{1, 1}, {1, 3}, {2, 1}, {2, 2}, {2, 3}]
    assert MyMatrex.neighbours({5, 5}, {1, 3}) == [{1, 2}, {1, 4}, {2, 2}, {2, 3}, {2, 4}]
    assert MyMatrex.neighbours({5, 5}, {1, 4}) == [{1, 3}, {1, 5}, {2, 3}, {2, 4}, {2, 5}]
    # last row
    assert MyMatrex.neighbours({5, 5}, {5, 2}) == [{4, 1}, {4, 2}, {4, 3}, {5, 1}, {5, 3}]
    assert MyMatrex.neighbours({5, 5}, {5, 3}) == [{4, 2}, {4, 3}, {4, 4}, {5, 2}, {5, 4}]
    assert MyMatrex.neighbours({5, 5}, {5, 4}) == [{4, 3}, {4, 4}, {4, 5}, {5, 3}, {5, 5}]
    # first column
    assert MyMatrex.neighbours({5, 5}, {2, 1}) == [{1, 1}, {1, 2}, {2, 2}, {3, 1}, {3, 2}]
    assert MyMatrex.neighbours({5, 5}, {3, 1}) == [{2, 1}, {2, 2}, {3, 2}, {4, 1}, {4, 2}]
    assert MyMatrex.neighbours({5, 5}, {4, 1}) == [{3, 1}, {3, 2}, {4, 2}, {5, 1}, {5, 2}]
    # last column
    assert MyMatrex.neighbours({5, 5}, {2, 5}) == [{1, 4}, {1, 5}, {2, 4}, {3, 4}, {3, 5}]
    assert MyMatrex.neighbours({5, 5}, {3, 5}) == [{2, 4}, {2, 5}, {3, 4}, {4, 4}, {4, 5}]
    assert MyMatrex.neighbours({5, 5}, {4, 5}) == [{3, 4}, {3, 5}, {4, 4}, {5, 4}, {5, 5}]
  end

  test "elements in non-first/non-last row/column have eight neighbours" do
    assert MyMatrex.neighbours({5, 5}, {3, 3}) == [
             {2, 2},
             {2, 3},
             {2, 4},
             {3, 2},
             {3, 4},
             {4, 2},
             {4, 3},
             {4, 4}
           ]
  end
end
