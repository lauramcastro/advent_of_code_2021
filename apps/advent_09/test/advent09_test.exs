defmodule Advent09Test do
  use ExUnit.Case
  doctest Advent09

  test "answer to first challenge" do
    entries =
      Utils.get_input("priv/input", fn x ->
        String.codepoints(x) |> Enum.map(fn d -> String.to_integer(d) end)
      end)

    assert Advent09.risk_level(entries) == 494
  end

  test "answer to second challenge" do
    entries =
      Utils.get_input("priv/input", fn x ->
        String.codepoints(x) |> Enum.map(fn d -> String.to_integer(d) end)
      end)

    assert Advent09.top3_basins_product(entries) == 1_048_128
  end
end
