defmodule MyMatrex do
  @moduledoc """
  This is the PR I offered! (https://github.com/versilov/matrex/issues/30)
  """

  @doc """
  Retrieve neighbours of a given position in a Matrex matrix.
  Includes diagonal by default.
  """
  @spec neighbours(
          {non_neg_integer(), non_neg_integer()},
          {non_neg_integer(), non_neg_integer()},
          boolean()
        ) :: list({non_neg_integer(), non_neg_integer()})
  def neighbours(size, position, diagonal \\ true)

  # first row, first column
  def neighbours({_max_r, _max_c}, {r = 1, c = 1}, diagonal) do
    down = {r + 1, c}
    right = {r, c + 1}

    if diagonal do
      diag_dr = {r + 1, c + 1}
      [right, down, diag_dr]
    else
      [right, down]
    end
  end

  # first row, last column
  def neighbours({_max_r, max_c}, {r = 1, c = max_c}, diagonal) do
    down = {r + 1, c}
    left = {r, c - 1}

    if diagonal do
      diag_dl = {r + 1, c - 1}
      [left, diag_dl, down]
    else
      [left, down]
    end
  end

  # first row
  def neighbours({_max_r, _max_c}, {r = 1, c}, diagonal) do
    down = {r + 1, c}
    left = {r, c - 1}
    right = {r, c + 1}

    if diagonal do
      diag_dl = {r + 1, c - 1}
      diag_dr = {r + 1, c + 1}
      [left, right, diag_dl, down, diag_dr]
    else
      [left, right, down]
    end
  end

  # last row, first column
  def neighbours({max_r, _max_c}, {r = max_r, c = 1}, diagonal) do
    up = {r - 1, c}
    right = {r, c + 1}

    if diagonal do
      diag_ur = {r - 1, c + 1}
      [up, diag_ur, right]
    else
      [up, right]
    end
  end

  # last row, last column
  def neighbours({max_r, max_c}, {r = max_r, c = max_c}, diagonal) do
    up = {r - 1, c}
    left = {r, c - 1}

    if diagonal do
      diag_ul = {r - 1, c - 1}
      [diag_ul, up, left]
    else
      [up, left]
    end
  end

  # last row
  def neighbours({max_r, _max_c}, {r = max_r, c}, diagonal) do
    up = {r - 1, c}
    left = {r, c - 1}
    right = {r, c + 1}

    if diagonal do
      diag_ur = {r - 1, c + 1}
      diag_ul = {r - 1, c - 1}
      [diag_ul, up, diag_ur, left, right]
    else
      [up, left, right]
    end
  end

  # first column
  def neighbours({_max_r, _max_c}, {r, c = 1}, diagonal) do
    up = {r - 1, c}
    down = {r + 1, c}
    right = {r, c + 1}

    if diagonal do
      diag_ur = {r - 1, c + 1}
      diag_dr = {r + 1, c + 1}
      [up, diag_ur, right, down, diag_dr]
    else
      [up, right, down]
    end
  end

  # last column
  def neighbours({_max_r, max_c}, {r, c = max_c}, diagonal) do
    up = {r - 1, c}
    down = {r + 1, c}
    left = {r, c - 1}

    if diagonal do
      diag_ul = {r - 1, c - 1}
      diag_dl = {r + 1, c - 1}
      [diag_ul, up, left, diag_dl, down]
    else
      [up, left, down]
    end
  end

  # rest
  def neighbours({_max_r, _max_c}, {r, c}, diagonal) do
    up = {r - 1, c}
    down = {r + 1, c}
    left = {r, c - 1}
    right = {r, c + 1}

    if diagonal do
      diag_ur = {r - 1, c + 1}
      diag_ul = {r - 1, c - 1}
      diag_dr = {r + 1, c + 1}
      diag_dl = {r + 1, c - 1}
      [diag_ul, up, diag_ur, left, right, diag_dl, down, diag_dr]
    else
      [up, left, right, down]
    end
  end
end
