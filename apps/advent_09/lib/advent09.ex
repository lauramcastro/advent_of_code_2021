defmodule Advent09 do
  @moduledoc """
  Documentation for `Advent09`.
  """

  require Logger

  @doc """
  Sum the heights of the low point values.

  ## Examples

      iex> Advent09.risk_level([[2,1,9,9,9,4,3,2,1,0],
      ...>                      [3,9,8,7,8,9,4,9,2,1],
      ...>                      [9,8,5,6,7,8,9,8,9,2],
      ...>                      [8,7,6,7,8,9,6,7,8,9],
      ...>                      [9,8,9,9,9,6,5,6,7,8]])
      15

  """
  @spec risk_level(list(list(non_neg_integer()))) :: non_neg_integer()
  def risk_level(areamap),
    do: low_points_values(areamap) |> Enum.map(fn x -> x + 1 end) |> Enum.sum()

  @doc """
  Retrieve the values of the low points.

  ## Examples

      iex> Advent09.low_points_values([[2,1,9,9,9,4,3,2,1,0],
      ...>                             [3,9,8,7,8,9,4,9,2,1],
      ...>                             [9,8,5,6,7,8,9,8,9,2],
      ...>                             [8,7,6,7,8,9,6,7,8,9],
      ...>                             [9,8,9,9,9,6,5,6,7,8]])
      [1,0,5,5]

  """
  @spec low_points_values(list(list(non_neg_integer()))) :: list(non_neg_integer())
  def low_points_values(areamap) do
    m = Matrex.new(areamap)
    low_points(m) |> Enum.map(fn {r, c} -> Matrex.at(m, r, c) |> trunc() end)
  end

  @doc """
  Locate the low points.

  ## Examples

      iex> Matrex.new([[2,1,9,9,9,4,3,2,1,0],
      ...>             [3,9,8,7,8,9,4,9,2,1],
      ...>             [9,8,5,6,7,8,9,8,9,2],
      ...>             [8,7,6,7,8,9,6,7,8,9],
      ...>             [9,8,9,9,9,6,5,6,7,8]]) |> Advent09.low_points()
      [{1,2},{1,10},{3,3},{5,7}]

  """
  @spec low_points(Matrex.t()) :: list({non_neg_integer(), non_neg_integer()})
  def low_points(m) do
    {rows, columns} = Matrex.size(m)

    for(r <- 1..rows, do: for(c <- 1..columns, do: {r, c}))
    |> List.flatten()
    |> Task.async_stream(fn x -> {x, is_lower_than_neighbours?(m, {rows, columns}, x)} end)
    |> Enum.flat_map(fn
      {:ok, {_, false}} -> []
      {:ok, {p, true}} -> [p]
    end)
  end

  defp is_lower_than_neighbours?(m, size, p = {r, c}) do
    value = Matrex.at(m, r, c)

    MyMatrex.neighbours(size, p, false)
    |> Enum.map(fn {nr, nc} -> Matrex.at(m, nr, nc) end)
    |> Enum.all?(fn nv -> value < nv end)
  end

  @doc """
  Retrieve the product of the top three largest basin sizes.

  ## Examples

      iex> Advent09.top3_basins_product([[2,1,9,9,9,4,3,2,1,0],
      ...>                               [3,9,8,7,8,9,4,9,2,1],
      ...>                               [9,8,5,6,7,8,9,8,9,2],
      ...>                               [8,7,6,7,8,9,6,7,8,9],
      ...>                               [9,8,9,9,9,6,5,6,7,8]])
      1134
      
  """
  @spec top3_basins_product(list(list(non_neg_integer()))) :: non_neg_integer()
  def top3_basins_product(areamap) do
    [a, b, c | _rest] =
      basins(areamap) |> Enum.map(fn b -> Enum.count(b) end) |> Enum.sort() |> Enum.reverse()

    a * b * c
  end

  @doc """
  Retrieve the basins from the low points.

  ## Examples

      iex> Advent09.basins([[2,1,9,9,9,4,3,2,1,0],
      ...>                  [3,9,8,7,8,9,4,9,2,1],
      ...>                  [9,8,5,6,7,8,9,8,9,2],
      ...>                  [8,7,6,7,8,9,6,7,8,9],
      ...>                  [9,8,9,9,9,6,5,6,7,8]])
      [[{1,1},{1,2},{2,1}],
       [{1,6},{1,7},{1,8},{1,9},{1,10},{2,7},{2,9},{2,10},{3,10}],
       [{2,3},{2,4},{2,5},{3,2},{3,3},{3,4},{3,5},{3,6},{4,1},{4,2},{4,3},{4,4},{4,5},{5,2}],
       [{3,8},{4,7},{4,8},{4,9},{5,6},{5,7},{5,8},{5,9},{5,10}]]
      
  """
  @spec basins(list(list(non_neg_integer()))) ::
          list(list({non_neg_integer(), non_neg_integer()}))
  def basins(areamap) do
    m = Matrex.new(areamap)

    low_points(m)
    |> Task.async_stream(fn p -> basin(m, p) end)
    |> Enum.map(fn {:ok, b} -> b end)
  end

  @doc """
  Retrieve the basin from a given point.

  ## Examples

      iex> Matrex.new([[2,1,9,9,9,4,3,2,1,0],
      ...>             [3,9,8,7,8,9,4,9,2,1],
      ...>             [9,8,5,6,7,8,9,8,9,2],
      ...>             [8,7,6,7,8,9,6,7,8,9],
      ...>             [9,8,9,9,9,6,5,6,7,8]]) |> Advent09.basin({1,2})
      [{1,1},{1,2},{2,1}]

      iex> Matrex.new([[2,1,9,9,9,4,3,2,1,0],
      ...>             [3,9,8,7,8,9,4,9,2,1],
      ...>             [9,8,5,6,7,8,9,8,9,2],
      ...>             [8,7,6,7,8,9,6,7,8,9],
      ...>             [9,8,9,9,9,6,5,6,7,8]]) |> Advent09.basin({1,10})
      [{1,6},{1,7},{1,8},{1,9},{1,10},{2,7},{2,9},{2,10},{3,10}]

      iex> Matrex.new([[2,1,9,9,9,4,3,2,1,0],
      ...>             [3,9,8,7,8,9,4,9,2,1],
      ...>             [9,8,5,6,7,8,9,8,9,2],
      ...>             [8,7,6,7,8,9,6,7,8,9],
      ...>             [9,8,9,9,9,6,5,6,7,8]]) |> Advent09.basin({3,3})
      [{2,3},{2,4},{2,5},{3,2},{3,3},{3,4},{3,5},{3,6},{4,1},{4,2},{4,3},{4,4},{4,5},{5,2}]

      iex> Matrex.new([[2,1,9,9,9,4,3,2,1,0],
      ...>             [3,9,8,7,8,9,4,9,2,1],
      ...>             [9,8,5,6,7,8,9,8,9,2],
      ...>             [8,7,6,7,8,9,6,7,8,9],
      ...>             [9,8,9,9,9,6,5,6,7,8]]) |> Advent09.basin({5,7})
      [{3,8},{4,7},{4,8},{4,9},{5,6},{5,7},{5,8},{5,9},{5,10}]
      
  """
  @spec basin(Matrex.t(), {non_neg_integer(), non_neg_integer()}) ::
          list({non_neg_integer(), non_neg_integer()})
  def basin(m, p) do
    size = Matrex.size(m)
    do_get_basin(m, size, MyMatrex.neighbours(size, p, false), [p])
  end

  defp do_get_basin(_m, _s, [], basin), do: basin |> Enum.sort()

  defp do_get_basin(m, s, [h = {r, c} | rest], basin) do
    if Matrex.at(m, r, c) < 9.0 do
      new_neighbours =
        MyMatrex.neighbours(s, h, false)
        |> Enum.filter(fn np -> not Enum.member?(rest, np) and not Enum.member?(basin, np) end)

      do_get_basin(m, s, rest ++ new_neighbours, [h | basin])
    else
      do_get_basin(m, s, rest, basin)
    end
  end
end
