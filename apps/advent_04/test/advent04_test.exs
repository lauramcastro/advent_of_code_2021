defmodule Advent04Test do
  use ExUnit.Case
  doctest Advent04

  test "answer to first challenge" do
    [numbers] = Utils.get_input("priv/input_numbers")
    numbers = String.split(numbers, ",") |> Enum.map(&String.to_integer(&1))
    boards = Utils.get_input("priv/input_boards") |> split_boards([])
    assert Advent04.bingo(numbers, boards, :win) == 29_440
  end

  test "answer to second challenge" do
    [numbers] = Utils.get_input("priv/input_numbers")
    numbers = String.split(numbers, ",") |> Enum.map(&String.to_integer(&1))
    boards = Utils.get_input("priv/input_boards") |> split_boards([])
    assert Advent04.bingo(numbers, boards, :lose) == 13_884
  end

  defp split_boards([], acc), do: acc

  defp split_boards([r1, r2, r3, r4, r5, "" | rest], acc) do
    row = for r <- [r1, r2, r3, r4, r5], do: String.split(r) |> Enum.map(&String.to_integer(&1))
    split_boards(rest, [row | acc])
  end
end
