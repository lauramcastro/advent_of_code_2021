defmodule Advent04 do
  @moduledoc """
  Documentation for `Advent04`.
  """

  @doc """
  Play bingo, either to win or to lose!

  ## Examples

      iex> Advent04.bingo([7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1],
      ...>                [[[22, 13, 17, 11,  0],
      ...>                  [ 8,  2, 23,  4, 24],
      ...>                  [21,  9, 14, 16,  7],
      ...>                  [ 6, 10,  3, 18,  5],
      ...>                  [ 1, 12, 20, 15, 19]],
      ...>                 [[ 3, 15,  0,  2, 22],
      ...>                  [ 9, 18, 13, 17,  5],
      ...>                  [19,  8,  7, 25, 23],
      ...>                  [20, 11, 10, 24,  4],
      ...>                  [14, 21, 16, 12,  6]],
      ...>                 [[14, 21, 17, 24,  4],
      ...>                  [10, 16, 15,  9, 19],
      ...>                  [18,  8, 23, 26, 20],
      ...>                  [22, 11, 13,  6,  5],
      ...>                  [ 2,  0, 12,  3,  7]]],
      ...>                :win)
      4512

      iex> Advent04.bingo([7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1],
      ...>                  [[[22, 13, 17, 11,  0],
      ...>                    [ 8,  2, 23,  4, 24],
      ...>                    [21,  9, 14, 16,  7],
      ...>                    [ 6, 10,  3, 18,  5],
      ...>                    [ 1, 12, 20, 15, 19]],
      ...>                   [[ 3, 15,  0,  2, 22],
      ...>                    [ 9, 18, 13, 17,  5],
      ...>                    [19,  8,  7, 25, 23],
      ...>                    [20, 11, 10, 24,  4],
      ...>                    [14, 21, 16, 12,  6]],
      ...>                   [[14, 21, 17, 24,  4],
      ...>                    [10, 16, 15,  9, 19],
      ...>                    [18,  8, 23, 26, 20],
      ...>                    [22, 11, 13,  6,  5],
      ...>                    [ 2,  0, 12,  3,  7]]],
      ...>                :lose)
      1924

  """
  @spec bingo(list(non_neg_integer()), list(list(list(non_neg_integer()))), :win | :lose) ::
          :game_over | integer()
  def bingo(draw, boards, mode) do
    board_processes = for b <- boards, do: BingoBoard.start_link(b)
    {number, sum} = play_bingo(draw, board_processes, mode)
    for {:ok, {p, _}} <- board_processes, do: BingoBoard.stop(p)
    number * sum
  end

  defp play_bingo([], _board_processes, _mode), do: :game_over

  defp play_bingo([n | rest], board_processes, mode) do
    task_results =
      Task.async_stream(board_processes, fn {:ok, {p, _status}} -> {p, BingoBoard.mark(p, n)} end)
      |> Enum.to_list()

    {done, pending} =
      task_results
      |> Enum.split_with(fn {:ok, {_p, board_status}} -> board_status != :continue end)

    do_play_bingo([n | rest], done, pending, mode)
  end

  defp do_play_bingo([_ | rest], [], pending, :win) do
    # we play to win but there are no winners yet
    play_bingo(rest, pending, :win)
  end

  defp do_play_bingo([n | _rest], [{:ok, {winner, _}}], _pending, :win) do
    # we play to win and there is a winner
    {n, BingoBoard.sum(winner)}
  end

  defp do_play_bingo([_n, m | rest], done, [{:ok, {loser, :continue}}], :lose) do
    # we play to lose and there is only one player left which hast not finished yet
    # so we iterate over it alone util it actually finishes
    pending = [{:ok, {loser, BingoBoard.mark(loser, m)}}]
    do_play_bingo([m | rest], done, pending, :lose)
  end

  defp do_play_bingo([n | _rest], _done, [{:ok, {loser, _status}}], :lose) do
    # we play to lose and there is only one player left which has just finished
    {n, BingoBoard.sum(loser)}
  end

  defp do_play_bingo([_ | rest], _done, pending, :lose) do
    # we play to lose and there is more than one player which have not finished
    play_bingo(rest, pending, :lose)
  end
end
