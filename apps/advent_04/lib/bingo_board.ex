defmodule BingoBoard do
  @moduledoc """
  Defines a bingo board as a process using a GenServer.
  """
  use GenServer
  require Matrax
  require Logger

  # API

  @doc """
  Starts the board.
  """
  @spec start_link(list(list(non_neg_integer))) :: {:ok, {pid(), :ready}}
  def start_link(data) do
    {:ok, p} = GenServer.start_link(__MODULE__, data)
    {:ok, {p, :ready}}
  end

  @doc """
  Marks a number on the board (turns it -1).
  """
  @spec mark(pid(), non_neg_integer()) :: :row | :column | :continue
  def mark(p, n) do
    GenServer.call(p, {:find_number, n})
  end

  @doc """
  Gets the sum of all the non-marked values on the board.
  """
  @spec sum(pid()) :: non_neg_integer()
  def sum(p) do
    GenServer.call(p, :sum_values)
  end

  @doc """
  Stops the board.
  """
  @spec stop(pid()) :: :ok
  def stop(p) do
    GenServer.stop(p)
  end

  # Callbacks

  @impl true
  def init(board_data) do
    {:ok, Matrax.new(board_data)}
  end

  @impl true
  def handle_call(:sum_values, _from, board) do
    {:reply, Matrax.to_list(board) |> Enum.filter(&unmarked/1) |> Enum.sum(), board}
  end

  def handle_call({:find_number, n}, _from, board) do
    case Matrax.find(board, n) do
      nil ->
        Logger.debug("Board #{inspect(self())} does not contain #{n}")
        {:reply, :continue, board}

      pos = {row, column} ->
        Matrax.put(board, pos, -1)

        status =
          report_status(
            Matrax.row_to_list(board, row) |> Enum.all?(&marked?/1),
            Matrax.column_to_list(board, column) |> Enum.all?(&marked?/1)
          )

        Logger.debug("Board #{inspect(self())} contains #{n}, new status is #{status}")

        {:reply, status, board}
    end
  end

  defp unmarked(n), do: not marked?(n)

  defp marked?(-1), do: true
  defp marked?(_), do: false

  defp report_status(true, _), do: :row
  defp report_status(_, true), do: :column
  defp report_status(false, false), do: :continue
end
