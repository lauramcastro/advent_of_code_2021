defmodule Advent02Test do
  use ExUnit.Case
  doctest Advent02

  test "answer to first challenge" do
    moves = Utils.get_input("priv/input", fn x -> Advent02.parse_movement(x) end)
    {horizontal, depth} = Advent02.calculate_position(moves)

    assert horizontal * depth == 2_150_351
  end

  test "answer to second challenge" do
    moves = Utils.get_input("priv/input", fn x -> Advent02.parse_movement(x) end)
    {horizontal, depth, _aim} = Advent02.calculate_position_with_aim(moves)

    assert horizontal * depth == 1_842_742_223
  end
end
