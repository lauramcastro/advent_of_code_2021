defmodule Advent02 do
  @moduledoc """
  Documentation for `Advent02`.
  """

  @doc """
  Calculate final position after following a planned course.

  ## Examples

      iex> Advent02.calculate_position([{:forward, 5},
      ...>                              {:down, 5},
      ...>                              {:forward, 8},
      ...>                              {:up, 3},
      ...>                              {:down, 8},
      ...>                              {:forward, 2}])
      {15, 10}

  """
  @spec calculate_position(list({atom(), integer()})) :: {integer(), integer()}
  def calculate_position(moves) do
    do_calculate_position({0, 0}, moves)
  end

  defp do_calculate_position(position, []), do: position

  defp do_calculate_position({horizontal, depth}, [{:forward, n} | more_moves]),
    do: do_calculate_position({horizontal + n, depth}, more_moves)

  defp do_calculate_position({horizontal, depth}, [{:down, n} | more_moves]),
    do: do_calculate_position({horizontal, depth + n}, more_moves)

  defp do_calculate_position({horizontal, depth}, [{:up, n} | more_moves]),
    do: do_calculate_position({horizontal, depth - n}, more_moves)

  @doc """
  Calculate final position after following a planned course, taking aim into account.

  ## Examples

      iex> Advent02.calculate_position_with_aim([{:forward, 5},
      ...>                                       {:down, 5},
      ...>                                       {:forward, 8},
      ...>                                       {:up, 3},
      ...>                                       {:down, 8},
      ...>                                       {:forward, 2}])
      {15, 60, 10}

  """
  @spec calculate_position_with_aim(list({atom(), integer()})) ::
          {integer(), integer(), integer()}
  def calculate_position_with_aim(moves) do
    do_calculate_position_with_aim({0, 0, 0}, moves)
  end

  defp do_calculate_position_with_aim(position, []), do: position

  defp do_calculate_position_with_aim({horizontal, depth, aim}, [{:forward, n} | more_moves]),
    do: do_calculate_position_with_aim({horizontal + n, depth + aim * n, aim}, more_moves)

  defp do_calculate_position_with_aim({horizontal, depth, aim}, [{:down, n} | more_moves]),
    do: do_calculate_position_with_aim({horizontal, depth, aim + n}, more_moves)

  defp do_calculate_position_with_aim({horizontal, depth, aim}, [{:up, n} | more_moves]),
    do: do_calculate_position_with_aim({horizontal, depth, aim - n}, more_moves)

  @doc """
  Parses a movement command.

  ## Examples

      iex> Advent02.parse_movement("forward 5")
      {:forward, 5}

  """
  @spec parse_movement(String.t()) :: {atom(), integer()}
  def parse_movement(movement) do
    [command, amount] = String.split(movement)
    {String.to_atom(command), String.to_integer(amount)}
  end
end
