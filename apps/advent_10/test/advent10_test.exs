defmodule Advent10Test do
  use ExUnit.Case
  doctest Advent10

  test "answer to first challenge" do
    entries = Utils.get_input("priv/input")

    assert Advent10.error_score(entries) == 358_737
  end

  test "answer to second challenge" do
    entries = Utils.get_input("priv/input")

    assert Advent10.middle_fix(entries) == 4_329_504_793
  end
end
