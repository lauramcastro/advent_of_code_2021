defmodule Advent10 do
  @moduledoc """
  Documentation for `Advent10`.
  """

  require Logger

  @doc """
  Pick the middle fix by score.

  ## Examples

      iex> Advent10.middle_fix(["[({(<(())[]>[[{[]{<()<>>",
      ...>                      "[(()[<>])]({[<{<<[]>>(",
      ...>                      "{([(<{}[<>[]}>{[]{[(<()>",
      ...>                      "(((({<>}<{<{<>}{[]{[]{}",
      ...>                      "[[<[([]))<([[{}[[()]]]",
      ...>                      "[{[{({}]{}}([{[{{{}}([]",
      ...>                      "{<[[]]>}<{[{[{[]{()[[[]",
      ...>                      "[<(<(<(<{}))><([]([]()",
      ...>                      "<{([([[(<>()){}]>(<<{{",
      ...>                      "<{([{{}}[<[[[<>{}]]]>[]]"])
      288957

  """
  @spec middle_fix(list(String.t())) :: non_neg_integer()
  def middle_fix(list_of_chunks) do
    fscores = fix_scores(list_of_chunks) |> Enum.sort()
    {middle, _} = List.pop_at(fscores, div(length(fscores), 2))
    middle
  end

  @doc """
  Scores the fixes for a list of chunks.

  ## Examples

      iex> Advent10.fix_scores(["[({(<(())[]>[[{[]{<()<>>",
      ...>                     "[(()[<>])]({[<{<<[]>>(",
      ...>                     "{([(<{}[<>[]}>{[]{[(<()>",
      ...>                     "(((({<>}<{<{<>}{[]{[]{}",
      ...>                     "[[<[([]))<([[{}[[()]]]",
      ...>                     "[{[{({}]{}}([{[{{{}}([]",
      ...>                     "{<[[]]>}<{[{[{[]{()[[[]",
      ...>                     "[<(<(<(<{}))><([]([]()",
      ...>                     "<{([([[(<>()){}]>(<<{{",
      ...>                     "<{([{{}}[<[[[<>{}]]]>[]]"])
      [288957,5566,1480781,995444,294]
  """
  @spec fix_scores(list(String.t())) :: list(non_neg_integer())
  def fix_scores(list_of_chunks) do
    Task.async_stream(list_of_chunks, fn x -> analyze_chunk(x) end)
    |> Enum.reduce([], fn
      {:ok, {:ok, fix}}, acc ->
        acc ++ [fix]

      {:ok, {:error, _}}, acc ->
        acc
    end)
    |> Enum.map(fn f -> fix_score(f, 0) end)
  end

  defp fix_score([], score), do: score
  defp fix_score([c | rest], score), do: fix_score(rest, 5 * score + score(c, :fix))

  @doc """
  Scores a list of chunks.

  ## Examples

      iex> Advent10.error_score(["[({(<(())[]>[[{[]{<()<>>",
      ...>                       "[(()[<>])]({[<{<<[]>>(",
      ...>                       "{([(<{}[<>[]}>{[]{[(<()>",
      ...>                       "(((({<>}<{<{<>}{[]{[]{}",
      ...>                       "[[<[([]))<([[{}[[()]]]",
      ...>                       "[{[{({}]{}}([{[{{{}}([]",
      ...>                       "{<[[]]>}<{[{[{[]{()[[[]",
      ...>                       "[<(<(<(<{}))><([]([]()",
      ...>                       "<{([([[(<>()){}]>(<<{{",
      ...>                       "<{([{{}}[<[[[<>{}]]]>[]]"])
      26397
  """
  @spec error_score(list(String.t())) :: non_neg_integer()
  def error_score(list_of_chunks) do
    results = analyze_chunks(list_of_chunks)

    for c <- [")", "]", "}", ">"] do
      results[c] * score(c, :error)
    end
    |> Enum.sum()
  end

  defp score(")", :error), do: 3
  defp score("]", :error), do: 57
  defp score("}", :error), do: 1_197
  defp score(">", :error), do: 25_137
  defp score(")", :fix), do: 1
  defp score("]", :fix), do: 2
  defp score("}", :fix), do: 3
  defp score(">", :fix), do: 4

  @doc """
  Analyzes a list of chunks.

  ## Examples

      iex> Advent10.analyze_chunks(["[({(<(())[]>[[{[]{<()<>>",
      ...>                          "[(()[<>])]({[<{<<[]>>(",
      ...>                          "{([(<{}[<>[]}>{[]{[(<()>",
      ...>                          "(((({<>}<{<{<>}{[]{[]{}",
      ...>                          "[[<[([]))<([[{}[[()]]]",
      ...>                          "[{[{({}]{}}([{[{{{}}([]",
      ...>                          "{<[[]]>}<{[{[{[]{()[[[]",
      ...>                          "[<(<(<(<{}))><([]([]()",
      ...>                          "<{([([[(<>()){}]>(<<{{",
      ...>                          "<{([{{}}[<[[[<>{}]]]>[]]"])
      %{")" => 2, "]" => 1, "}" => 1, ">" => 1}
  """
  @spec analyze_chunks(list(String.t())) :: map()
  def analyze_chunks(list_of_chunks) do
    Task.async_stream(list_of_chunks, fn x -> analyze_chunk(x) end)
    |> Enum.reduce(%{}, fn
      {:ok, {:ok, _fix}}, acc ->
        acc

      {:ok, {:error, {_expected, found}}}, acc ->
        Map.update(acc, found, 1, fn n -> n + 1 end)
    end)
  end

  @doc """
  Analyzes one chunk.

  ## Examples

      iex> Advent10.analyze_chunk("{([(<{}[<>[]}>{[]{[(<()>")
      {:error, {"]","}"}}

      iex> Advent10.analyze_chunk("[[<[([]))<([[{}[[()]]]")
      {:error, {"]",")"}}

      iex> Advent10.analyze_chunk("[{[{({}]{}}([{[{{{}}([]")
      {:error, {")","]"}}

      iex> Advent10.analyze_chunk("[<(<(<(<{}))><([]([]()")
      {:error, {">",")"}}

      iex> Advent10.analyze_chunk("<{([([[(<>()){}]>(<<{{")
      {:error, {"]",">"}}

  """
  @spec analyze_chunk(String.t()) :: :ok | {:error, {String.t(), String.t()}}
  def analyze_chunk(chunk) do
    case String.codepoints(chunk) |> do_analyze([]) do
      {:end_of_chunk, acc} ->
        fix = inverse(acc, [])
        Logger.debug("We could fix this using #{inspect(fix)}")
        {:ok, fix}

      output = {expected, found} ->
        Logger.debug("Expected #{inspect(expected)}, but found #{inspect(found)} instead")
        {:error, output}
    end
  end

  defp do_analyze([], acc), do: {:end_of_chunk, acc}
  defp do_analyze([c | more], []), do: do_analyze(more, [c])
  defp do_analyze([")" | more], ["(" | acc]), do: do_analyze(more, acc)
  defp do_analyze(["]" | more], ["[" | acc]), do: do_analyze(more, acc)
  defp do_analyze(["}" | more], ["{" | acc]), do: do_analyze(more, acc)
  defp do_analyze([">" | more], ["<" | acc]), do: do_analyze(more, acc)

  defp do_analyze([c | more], [d | acc]) do
    if is_closing?(c) and is_opening?(d) do
      {closes(d), c}
    else
      do_analyze(more, [c, d | acc])
    end
  end

  defp is_opening?(c) when c == "(" or c == "[" or c == "{" or c == "<", do: true
  defp is_opening?(_), do: false

  defp is_closing?(c) when c == ")" or c == "]" or c == "}" or c == ">", do: true
  defp is_closing?(_), do: false

  defp closes("("), do: ")"
  defp closes("["), do: "]"
  defp closes("{"), do: "}"
  defp closes("<"), do: ">"

  defp inverse([], acc), do: Enum.reverse(acc)
  defp inverse([c | rest], acc), do: inverse(rest, [closes(c) | acc])
end
